# nebula
> a collection of utilities for content creation and community management

Created for the sake of learning (and memes)

[![Build status](https://gitlab.com/cosban/nebula/badges/master/build.svg)](https://gitlab.com/cosban/nebula/commits/master) 
[![GoDoc](https://godoc.org/github.com/cosban/nebula?status.svg)](https://godoc.org/github.com/cosban/nebula)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Nebula acts as a central hub and api for a set of plugins. Right now, Nebula's primary use case is to run as a webserver
that is able to host pages which may or may not require persistent data.

The goal of this project is to reduce and simplify the steps needed to get one's personal site live. Nebula can be used 
to operate forums, CRUD apps, static sites, or sales portals. 

## Prerequisites

* GNU/Linux or Docker
* [nginx](httpsnginx.org)/[Apache](https://apache.org) (optional)
* [postgres ](https://www.postgresql.org) (if bundled plugins are used)

## Usage

Nebula requires Linux if you wish to run it with the least setup. If you wish to run nebula on a windows machine, Docker
may be used. I have provided a `Dockerfile` for this purpose.

### Installation

First, get and install `nebula`:

`go get gitlab.com/cosban/nebula`

Then, plugins. Nebula comes bundled with `magnetar`, `sol`, `rover`, and `aurora` which act as the webserver, user 
authentication, robot email, and user permissioning services respectively.

Plugins must be built and "plugged in" in order to be used:

For the sake of example, we will use `magnetar` and run our project at `/var/www/`

```
$ cd $GOPATH/src/gitlab.com/cosban/magnetar/main
$ go build -buildmode=plugin -o magnetar.so
$ mkdir -p /var/www/plugins
$ mv magnetar /var/www/plugins
```

You will then want to perform any configuration that is necessary for the plugin to be used. This is typically done by 
editing a `config.json` file at the root of where the project is run.

If docker is used, the `Dockerfile` automatically sets up (without creating a `config.json`) the bundled plugins

```
$ cd $GOPATH/src/gitlab.com/cosban/nebula
$ docker build -t nebula:latest .
```

### Configuration

The default location for is the root of the project as `config.json`

```
{
    "debug": true,
    "host": {
        "bind": "0.0.0.0:80",
        "hostname": "example.com",
        "cookies": "makethisareallylongandrandomstring"
    }
}
```

Here's what each option does:

`debug` : optional, enable or disable debug. Default is false (off).  
`bind` : what the webserver binds to, if run behind a reverse proxy (nginx/apache), you may want to set it to `":8000"`  
`hostname` : the domain name or ip is this run on  
`cookies` : set this to a really long and random string, keep it secret. Reset it if you wish to de-activate all 
currently valid sessions.

### Running

On linux (with bundled plugins and dev mode enabled):
```
$ cd /var/www/
$ go run nebula
    [DEBUG]: The server is currently running in debug mode due to a setting in the config file.
    [DEBUG]: [Nebula] Loading plugin modules
    [DEBUG]: [Nebula] Loading module: magnetar.so
    [DEBUG]: [Magnetar] is set up
    [DEBUG]: [Nebula] Loading module: rover.so
    [DEBUG]: [Rover] is set up
    [DEBUG]: [Nebula] Loading module: sol.so
    [DEBUG]: [Sol] connecting to db
```

With Docker 
```
$ docker run -v /var/www/templates:/data -v /var/www:/config -p 80:80 nebula
    [DEBUG]: The server is currently running in debug mode due to a setting in the config file.
    [DEBUG]: [Nebula] Loading plugin modules
    [DEBUG]: [Nebula] Loading module: magnetar.so
    [DEBUG]: [Magnetar] is set up
    [DEBUG]: [Nebula] Loading module: rover.so
    [DEBUG]: [Rover] is set up
    [DEBUG]: [Nebula] Loading module: sol.so
    [DEBUG]: [Sol] connecting to db
```

## Contribution
Contributions are welcome. I haven't created a set of contribution guidelines yet. 

If you wish to contribute, you may do the following:

### Existing issues

1. Find an issue labeled `help-accepted`
2. Do your work
3. Create a pull request

### Creating an issue / Fixing existing bugs

1. Discover something you would like to add or fix
2. Create an issue
3. Wait for it to be labeled `accepted` and assigned to you
4. Do your work
5. Create a pull request

Contribution to this project is not a right. Acting in a way that may connect negative behavior to this project will 
result in being banned from contributing.

## License 

The MIT License (MIT) 2018 - [Buckley](https://gitlab.com/cosban/). See 
[LICENSE](https://gitlab.com/cosban/nebula/blob/master/LICENSE) for more details