module gitlab.com/cosban/nebula

require (
	github.com/lib/pq v1.1.1 // indirect
	gitlab.com/cosban/config v0.0.0-20181013175647-04c8029c149f
	gitlab.com/cosban/core v0.0.0-20190514023631-9d71b2b620ec
	gitlab.com/cosban/dev v1.0.0
	gitlab.com/cosban/persistence v0.2.0
)
