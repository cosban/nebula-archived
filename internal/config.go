package internal

type Configuration struct {
	Plugins []string `json:"plugins" optional:"true"`
	Sql     struct {
		Host     string `json:"host"`
		Port     int    `json:"port"`
		Password string `json:"password" optional:"true"`
		Database string `json:"database"`
		Username string `json:"username"`
		Sslmode  string `json:"sslmode" values:"disable,allow,prefer,require,verify-ca,verify-full"`
	} `json:"sql"`
}

var Config Configuration
