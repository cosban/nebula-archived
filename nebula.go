package main // import "gitlab.com/cosban/nebula"

import (
	"fmt"
	"io/ioutil"
	"log"
	"plugin"
	"runtime"

	_ "gitlab.com/cosban/persistence/postgres"

	"gitlab.com/cosban/config"
	"gitlab.com/cosban/core"
	"gitlab.com/cosban/core/hub"
	"gitlab.com/cosban/dev"
	"gitlab.com/cosban/nebula/internal"
)

var (
	nebula  *Nebula
	plugins = "plugins"
)

type Nebula struct {
	hub *hub.Hub
}

func main() {
	if err := config.ReadConfigAt(&internal.Config, core.ConfigPath); err != nil {
		log.Fatalf("[Nebula] Unable to read config file at %s", core.ConfigPath)
	}
	_ = GetNebula()
	dev.Log("[Nebula] Initializing Workers")

	nebula.hub.DoWork()
}

func GetNebula() *Nebula {
	if nebula == nil {
		nebula = &Nebula{
			hub: hub.New(),
		}
		nebula.hub.InitializeDatabase(
			"postgres",
			fmt.Sprintf(core.DBConnection,
				internal.Config.Sql.Username,
				internal.Config.Sql.Password,
				internal.Config.Sql.Host,
				internal.Config.Sql.Port,
				internal.Config.Sql.Database,
				internal.Config.Sql.Sslmode,
			),
		)
		if runtime.GOOS == "linux" {
			dev.Log("[Nebula] Loading plugin modules")
			nebula.setPluginControllers()
		} else {
			log.Fatal("[Nebula] Please install linux.")
		}
		var err error

		if err != nil {
			log.Fatal("[Nebula] Failed to connect to database")
		}
	}
	return nebula
}

func (n *Nebula) setPluginControllers() {
	files, err := ioutil.ReadDir(plugins)
	if err != nil {
		log.Fatal(err)
	}
	if len(files) < 1 {
		log.Fatal("[Nebula] There are no plugins")
	}
	for _, f := range files {
		p, err := plugin.Open(fmt.Sprintf("%s/%s", plugins, f.Name()))
		if err != nil {
			dev.Error(err)
			return
		}
		if build, err := p.Lookup(core.Modulizer); err == nil {
			dev.Logf("[Nebula] Loading module: %s", f.Name())
			module := build.(func(*hub.Hub) core.Module)(n.hub)
			dev.Logf("[%s] module is ready", module.Name())
		}
	}
}
